/* Require Plugins */
var gulp = require('gulp'),
    path = require('path'),
    browserSync = require('browser-sync').create(),
    bs1 = require('browser-sync').create('mustache'), // Create a named instance
    argv = require('minimist')(process.argv.slice(2)),
    svgSprite = require('gulp-svg-sprite'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    sassGlob = require('gulp-sass-glob'),
    autoprefixer = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    uglify = require('gulp-uglify'),
    mustache = require("gulp-mustache");

function resolvePath(pathInput) {
  return path.resolve(pathInput).replace(/\\/g,"/");
}
 
/* Setup Configs */
var watching = false;
var spriteConfig = {
    shape: {
        spacing: {
            padding: 2
        }
    },
    mode: {
        css: {
            bust: false,
            prefix: 'icon-%s',
            dest: 'public',
            sprite: 'images/sprites.svg',
            example: {
                dest: '../source/_patterns/00-base/03-images/01-icons.mustache',
                template: 'source/images/svg-sprites/template.html'
            },
            render: {
                scss: {
                    dest: '../source/css/base/_sprites.scss',
                    template: 'source/images/svg-sprites/_sprites.scss'
                }
            }
        }
    }
};
var sassOptions = {
 errLogToConsole: true,
 outputStyle: 'compressed'
};
var autoprefixOptions = {
  browsers: ['last 2 versions', 'IE 10','']
};

/* Handle Compass Errors breaking the watch task */ 
function onError(err) {
  console.log(err.toString());
  if (watching) {
    this.emit('end');
  } else {
    process.exit(1);
  }
}

// gulp.task('serve', ['sass'], function() {
//     gulp.watch(paths.css, ['sass']);
// });

// minify images
gulp.task('imagemin', function () {
   return gulp.src(paths().source.images)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(paths().public.images));
});

// Compress js
gulp.task('uglify', function() {
    return gulp.src(resolvePath(paths().source.js + '**/*.js'))
    .pipe(uglify().on('error', onError))
    .pipe(gulp.dest(paths().public.root));
});

bs1.init({
    port: 3030,
    server: "./public/content"
});

gulp.task('sass', function() {
  return gulp
    .src(path.resolve(paths().source.scss, '**/*.scss'))
    .pipe(sassGlob())
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(autoprefixer(autoprefixOptions))
    .pipe(gulp.dest(path.resolve(paths().public.css)))
    .pipe(gulp.dest(path.resolve(paths().public.mustache)))
    .pipe(bs1.stream());
});
 
// build sprites
gulp.task('sprites', function() {
    return gulp.src(paths().source.sprites)
        .pipe(svgSprite(spriteConfig))
        .pipe(gulp.dest('./'))
        .pipe(browserSync.stream())
        .pipe(bs1.stream());
});

// Mustache compile
gulp.task('mustache', function () {
    return gulp
      .src(path.resolve(paths().source.mustache, 'templates/*.html'))
      .pipe(mustache('source/mustache/data/data.json',{},{}))
      .pipe(gulp.dest(path.resolve(paths().public.mustache)))
      .pipe(bs1.stream());
});

//// FROM PATTERN LAB ////
gulp.task('pl-copy:styleguide', function(){
  return gulp.src(resolvePath(paths().source.styleguide) + '/**/!(*.css)')
    .pipe(gulp.dest(resolvePath(paths().public.root)))
    .pipe(browserSync.stream())
    .pipe(bs1.stream());
});

// Styleguide Copy and flatten css
gulp.task('pl-copy:styleguide-css', function(){
  return gulp.src(resolvePath(paths().source.styleguide) + '/**/*.css')
    .pipe(gulp.dest(function(file){
      //flatten anything inside the styleguide into a single output dir per http://stackoverflow.com/a/34317320/1790362
      file.path = path.join(file.base, path.basename(file.path));
      return resolvePath(path.join(paths().public.styleguide, '/css'));
    }))
    .pipe(browserSync.stream())
    .pipe(bs1.stream());
});


/******************************************************
 * PATTERN LAB CONFIGURATION - API with core library
******************************************************/
//read all paths from our namespaced config file
var config = require('./patternlab-config.json'),
  patternlab = require('patternlab-node')(config);

function paths() {
  return config.paths;
}

function getConfiguredCleanOption() {
  return config.cleanPublic;
}

function build(done) {
  patternlab.build(done, getConfiguredCleanOption());
}

gulp.task('pl-assets', gulp.series(
  gulp.parallel(
    'imagemin',
    'pl-copy:styleguide',
    'pl-copy:styleguide-css'
  ),
  function(done){
    done();
  })
);

gulp.task('patternlab:version', function (done) {
  patternlab.version();
  done();
});

gulp.task('patternlab:help', function (done) {
  patternlab.help();
  done();
});

gulp.task('patternlab:patternsonly', function (done) {
  patternlab.patternsonly(done, getConfiguredCleanOption());
});

gulp.task('patternlab:liststarterkits', function (done) {
  patternlab.liststarterkits();
  done();
});

gulp.task('patternlab:loadstarterkit', function (done) {
  patternlab.loadstarterkit(argv.kit, argv.clean);
  done();
});

gulp.task('patternlab:build', gulp.series('pl-assets', build, function(done){
  done();
}));

gulp.task('patternlab:installplugin', function (done) {
  patternlab.installplugin(argv.plugin);
  done();
});

/******************************************************
 * SERVER AND WATCH TASKS
******************************************************/
// watch task utility functions
function getSupportedTemplateExtensions() {
  var engines = require('./node_modules/patternlab-node/core/lib/pattern_engines');
  return engines.getSupportedFileExtensions();
}
function getTemplateWatches() {
  return getSupportedTemplateExtensions().map(function (dotExtension) {
    return resolvePath(paths().source.patterns) + '/**/*' + dotExtension;
  });
}

function reload() {
  browserSync.reload()
  bs1.reload();
}

function reloadCSS() {
  browserSync.reload('*.css');
}

function watch() {
  gulp.watch(path.resolve(paths().source.scss, '**/*.scss'), { awaitWriteFinish: true }).on('change', gulp.series('sass', reloadCSS));
  gulp.watch(resolvePath(paths().source.styleguide) + '/**/*.*', { awaitWriteFinish: true }).on('change', gulp.series('pl-copy:styleguide', 'pl-copy:styleguide-css', reloadCSS));
  gulp.watch(path.resolve(paths().source.sprites, '*.svg'), gulp.parallel('sprites'));
  gulp.watch(path.resolve(paths().source.images, '*.*'), gulp.parallel('sprites'));
  gulp.watch(path.resolve(paths().source.js, '**/*.js'), gulp.parallel('uglify'));
  gulp.watch(path.resolve(paths().source.mustache, 'templates/*.html'), gulp.parallel('mustache'));

  var patternWatches = [
    resolvePath(paths().source.patterns) + '/**/*.json',
    resolvePath(paths().source.patterns) + '/**/*.md',
    resolvePath(paths().source.data) + '/*.json',
    resolvePath(paths().source.fonts) + '/*',
    resolvePath(paths().source.images) + '/*',
    resolvePath(paths().source.meta) + '/*',
    resolvePath(paths().source.annotations) + '/*'
  ].concat(getTemplateWatches());

  gulp.watch(patternWatches, { awaitWriteFinish: true }).on('change', gulp.series(build, reload));
}

gulp.task('patternlab:connect', gulp.series(function(done) {
  browserSync.init({
    server: {
      baseDir: resolvePath(paths().public.root)
    },
    snippetOptions: {
      // Ignore all HTML files within the templates folder
      blacklist: ['/index.html', '/', '/?*']
    },
    notify: {
      styles: [
        'display: none',
        'padding: 15px',
        'font-family: sans-serif',
        'position: fixed',
        'font-size: 1em',
        'z-index: 9999',
        'bottom: 0px',
        'right: 0px',
        'border-top-left-radius: 5px',
        'background-color: #1B2032',
        'opacity: 0.4',
        'margin: 0',
        'color: white',
        'text-align: center'
      ]
    }
  }, function(){
    console.log('PATTERN LAB NODE WATCHING FOR CHANGES');
    done();
  });
}));

/******************************************************
 * COMPOUND TASKS
******************************************************/
gulp.task('default', gulp.series('patternlab:build'));
gulp.task('watch', gulp.series('patternlab:build', watch));
gulp.task('serve', gulp.series('patternlab:build', 'patternlab:connect', watch));




// Watch Files For Changes
// gulp.task('watch', function() {
//     // not including css watch because it's already in the serve task
//     watching = true;
//     gulp.watch(paths.js, ['js']);
//     gulp.watch([paths.images, paths.sprites], ['build-images']);
// });
 
// // Default Task
// gulp.task('default', ['serve', 'watch']);