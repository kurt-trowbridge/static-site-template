'use strict';
(function($){
  $().ready(function(){

    function disableTabs() {
      $('.main-nav__list > li > a').each( function(){
        $(this).attr('tabindex','-1');
      });
    }

    disableTabs();

    // Toggle Main Menu
    $('[data-menutoggle]').on('touch click', function(e) {
      e.preventDefault();
      $('[data-menu="main-menu"]').toggleClass('open');
      $('html,body').toggleClass('open');
    });

    // bind a click event to the 'skip' link
    $(".skip-nav").on('click',function(event){

        // strip the leading hash and declare
        // the content we're skipping to
        var skipTo="#"+this.href.split('#')[1];

        // Setting 'tabindex' to -1 takes an element out of normal
        // tab flow but allows it to be focused via javascript
        $(skipTo).attr('tabindex', -1).on('blur focusout', function () {

            // when focus leaves this element,
            // remove the tabindex attribute
            $(this).removeAttr('tabindex');

        }).focus(); // focus on the content container
    });

    /* Close nav by clicking off from it */
    $(document).on('click', function(event) {
        /* I want every element in my document, except the stuff inside of #dnn_HelpPane to trigger the click event */
        if (!$(event.target).closest('.main-nav__item').length) {
            /* This is the stuff I want to happen, to close things, removing open classes, adjusting my aria roles, etc */
            $('.main-nav__item, .main-nav__list').removeClass('open');
        }
    });


  });
})(jQuery);